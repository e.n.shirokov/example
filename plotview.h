#ifndef PLOTVIEW_H
#define PLOTVIEW_H

#include <QObject>
#include <QGraphicsView>
#include <QScrollBar>
#include <QMouseEvent>
#include <QtDebug>
#include <QRubberBand>
#include <QKeyEvent>
#include <QTimer>
#include <QClipboard>
#include <QMimeData>

#include "scaleitem.h"
#include "pointitem.h"
#include "plotitem.h"
#include "pasteareaitem.h"
#include "effects/effect_static_color.h"
#include "pointcolormenu.h"
#include "cursoritem.h"

// Канал (график функции канала RGB)
struct Channel
{
    Channel() : _id(0) {}

    Channel(int id, const QString &name, const QColor &color, const QVector<PointItem*>& points)
        : _id(id),
          _name(name),
          _color(color),
          _points(points)
    {}

    int _id;
    QString _name;
    QColor _color;
    QVector<PointItem*> _points; // точки грфика

};

const int CURSOR_RECT = 10;
const double SCALE_FACTOR = 1.2;

enum PlotType{
    PLOT_INTENSITY,
    PLOT_RGB,

};

typedef QMap<QString, Channel*> ChannelMap;

class PlotGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    PlotGraphicsView(PlotType type, QWidget *parent = nullptr);

    enum MirrorType {
        MirrorHorizontal,
        MirrorVertical,
    } ;

    enum AlignType {
        AlignVertical,
        AlignHorizontal,
    };

    void setDuration(int value, bool isInProportion = false); // задать длительность сцены
    void setOffset(int value);
    void setData(int priority, const QColor& color, const QString& name, QVector<QPointF> &data); // установить точки для одного канала
    QVector<QPointF> &getData(const QString &name);     // получить точки для одного канала
    QList<QString> getPlotNames();                      // получить список каналов
    QColor getColorByName(const QString& name);         // получить цвет канала по его имени
    bool getSelectedData();                             // получить список выделенных точек
    void clear();                                       // очистить график
    bool isCursorCatched();                             // если курсор захвачен пользователем (мышкой)

    void start();
    void stop();
    void pause();

    qreal setCursorPosition(int ms);
    int   getCursorPositionMs();
    qreal getCursorPositionByMs(int ms);

    void setPasteReplace(bool val) { _isPasteReplace = val; }

    void copy();
    void paste();
    void pasteData(const QMap<QString, QPair<QColor, QVector<QPointF> > > &data, qreal leftBorder, qreal rightBorder);

    void setAlignment(AlignType type);


public slots:
    void setPosForSelectedPoint(double, double);
    void onPasteCancel();
    void onPasteAccept();
    void onPasteMirror(MirrorType type);

    void onMergePastedChannels(const QVector<QPair<QString, QString> > &settingsChannel);

signals:
    void selectedPoint(qreal, qreal);
    //void pointChanged();
    void NeedSave();
    void CursorMoved();
    void UpdateViewStatesPlease();
    void pastePrepared(QList<QString>, QList<QString>);
    void pasteDone();

    void store();


protected:
    void mouseDoubleClickEvent(QMouseEvent * event) override;
    void wheelEvent(QWheelEvent * event) override;
    void resizeEvent(QResizeEvent * event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

    void mousePressed(QMouseEvent *event, const QString &name, PointItem *point);


private slots:
    void onScrollBarChanged(int val);
    void onPointChanged();
    void onCurrentColorChanged(const QVector<QPair<QString, int>>& channel_states);
    //void onTimeout();

private:
    PointItem* findRightSelcetedPoint(PointItem* from);
    PointItem* findLeftSelcetedPoint(PointItem* from);
    PointItem* findTopSelectedPoint();
    PointItem* findBottomSelectedPoint();
    void checkPointForBorders(QPointF &pos, PointItem *point);
    QPointF checkPointForBorders2(QPointF point);
    void checkPointForNeighbor(QPointF &pos, PointItem *point);
    //void getChannelValueForPos(const QPointF &pos);
    void setCursorForRect(const QPointF& pos, const QRectF& rect);

private:
    PlotItem *gItem;

    CursorItem* _cursorItem;

    ScaleItem *scaleX;
    ScaleItem *scaleY;
    ScaleItem *scaleVertical;
    ScaleItem *scaleHorizontal;

    qreal _scaleFactorX;
    qreal _scaleFactorY;

    QRubberBand* _rubberBand;
    QPoint _originPos; // позиция курсора при нажатии клавиши мыши

    QSet<QGraphicsItem*> _selectedItems;
    QSet<QGraphicsItem*> _clipboard;
    PointItem* _selectedPoint;

    bool _isPressed;  // флаг нажатия кнопки (используется при перетаскивании объектов)
    bool _isCursorCatched;   // флаг захвата курсора
    bool _isPasteAreaCatched;

    ChannelMap _channelMap;
    QMap<QString, QPair<QColor, QVector<QPointF> > > _dataSelected;

    QMap<QString, QVector<PointItem*> > _pasteChannelMap;

    //QSet<int> _selectedChannels;

    QVector<QPointF> _points;

    int _posStartX;
    int _posStartY;

    QGraphicsItemGroup _group;

    PasteAreaItem* _pasteAreaItem;

    PlotType _plotType;

    lightcoder::EffectStaticColor* color_picker_;
    PointColorMenu* _pointColorMenu;

    QMap<QString, int> channels_map;
    QPointF _sceneToPlotPoint;

    QMap<QString, PointItem* > _pointsUnderCursor;

    QPointF pasteAreaPos; // позиция области вставки
    QPointF cursorPos; // позиция курсора на графике
    QTimer* _timer;
    bool _isPasteReplace;

    QPointF _diffPoint; // разница между позицией курсора и позицией точки (при перетаскивании)
    bool _isMoved {false};
    QPoint _cursorPos;

    //QVector<UndoCommand*> _undoStack;  // стек для сохранения синмков каналов


};


#endif // PLOTVIEW_H
