#include "plotview.h"
#include <QApplication>
#include <cmath>

#include "effects/script_panel.h"

const int LEFT_FILED_WIDTH   = 40;
const int RIGHT_FILED_WIDTH  = 40;
const int TOP_FILED_WIDTH    = 40;
const int BOTTOM_FILED_WIDTH = 40;

const QString DEFAULT_BRUSH = "#1B242F";

bool lessThan(QPointF one, QPointF two) {
     if (one.x() < two.x()) {
         return true;
     }
     if(one.x() == two.x() && one.y() < two.y()) {
         return true;
     }

     return false;
 };

PlotGraphicsView::PlotGraphicsView(PlotType type, QWidget *parent)
    : QGraphicsView(parent)
    , _selectedPoint(nullptr)
    , _isPressed(false)
    , _isCursorCatched(false)
    , _isPasteAreaCatched(false)
    , _plotType(type)
    , _cursorItem(nullptr)
    , _isPasteReplace(true)
{
    _diffPoint = QPointF(0, 0);

    //_pointColorMenu = new PointColorMenu;
    //_timer = new QTimer;
    //_timer->setInterval(100);
    //connect(_timer, &QTimer::timeout, this, &PlotGraphicsView::onTimeout);

    QGraphicsScene *_scene = new QGraphicsScene(QRectF(-40, -540, 1000, 1000));
    _scene->setBackgroundBrush(QBrush(QColor(DEFAULT_BRUSH)));

    this->setScene(_scene);

    _rubberBand = new QRubberBand(QRubberBand::Rectangle, this->viewport());

    setRenderHint(QPainter::Antialiasing);
    setRenderHint(QPainter::SmoothPixmapTransform);

    gItem = new PlotItem(PLOT_WIDTH, PLOT_HEIGHT, nullptr);

    this->scene()->addItem(gItem);

    this->scene()->setSceneRect(QRectF(0,
                                       0,
                                       gItem->sceneBoundingRect().width() + LEFT_FILED_WIDTH + RIGHT_FILED_WIDTH,
                                       gItem->sceneBoundingRect().height() + LEFT_FILED_WIDTH + LEFT_FILED_WIDTH));

    gItem->setPos(LEFT_FILED_WIDTH, LEFT_FILED_WIDTH);
    //gItem->setZValue(3);

    if(type == PlotType::PLOT_RGB){
        scaleX = new ScaleItem(gItem, ScaleItem::Orientation::Horizontal, true);
        scaleX->setScaleMinimum(0);
        scaleX->setScaleMaximum(100);

        scaleY = new ScaleItem(gItem, ScaleItem::Orientation::Vertical);
        scaleY->setScaleMinimum(0);
        scaleY->setScaleMaximum(255);

        _cursorItem = new CursorItem;
        _cursorItem->setZValue(1);

        this->scene()->addItem(_cursorItem);

        _cursorItem->setPos(QPointF(LEFT_FILED_WIDTH, 0));

        _cursorItem->hide();

    }
    else if(type == PlotType::PLOT_INTENSITY) {
        scaleX = new ScaleItem(gItem, ScaleItem::Orientation::Horizontal);
        scaleX->setScaleMinimum(0);
        scaleX->setScaleMaximum(100);

        scaleY = new ScaleItem(gItem, ScaleItem::Orientation::Vertical);
        scaleY->setScaleMinimum(0);
        scaleY->setScaleMaximum(100);
    }

    scaleVertical = new ScaleItem(gItem, ScaleItem::Orientation::Vertical, false, true);

    scaleHorizontal = new ScaleItem(gItem, ScaleItem::Orientation::Horizontal, false, true);
    scaleHorizontal->setScaleMinimum(0);
    scaleHorizontal->setScaleMaximum(100);

    _scaleFactorX = 1;
    _scaleFactorY = 1;

    this->scene()->addItem(scaleX);
    this->scene()->addItem(scaleY);
    this->scene()->addItem(scaleHorizontal);
    this->scene()->addItem(scaleVertical);

    connect(this->horizontalScrollBar(), &QScrollBar::valueChanged, this, &PlotGraphicsView::onScrollBarChanged);
    connect(this->verticalScrollBar(), &QScrollBar::valueChanged, this, &PlotGraphicsView::onScrollBarChanged);

    this->update();
    this->setContentsMargins(0, 0, 0, 0);

    _pasteAreaItem = new PasteAreaItem(gItem);

    //this->scene()->addItem(_pasteAreaItem);

    _pasteAreaItem->hide();

    color_picker_ = new lightcoder::EffectStaticColor(false, this);
    connect(color_picker_, &lightcoder::EffectStaticColor::CurrentColorChanged,
            this, &PlotGraphicsView::onCurrentColorChanged);
    connect(color_picker_, &lightcoder::EffectStaticColor::hided, this, &PlotGraphicsView::store);

    this->setCursor(Qt::OpenHandCursor);
}

void PlotGraphicsView::setDuration(int value, bool isInProportion)
{
    if(value == 0) {
        return;
    }

    if(isInProportion == false) {
        // если выставлен флаг пропорционального изменения длительности сцены

        int oldScaleMax = scaleX->getScaleMaximum();
        qreal percentDifferent = (value - oldScaleMax) * 100.0 / value;
        qDebug() << "setDuration()";

        for(QString& key : _channelMap.keys()) {
            Channel* channel = _channelMap.value(key);

            if(channel != nullptr) {
                for(PointItem* point : channel->_points) {

                    if(point->isDisableX()) {
                        continue;
                    }

                    qreal percentPlotX = point->pos().x() * 100.0 / gItem->boundingRect().width();
                    qreal percentPlotXNew = percentPlotX * percentDifferent / 100.0;
                    qreal newPosX = (percentPlotX - percentPlotXNew) * gItem->boundingRect().width() / 100.0;

                    if(newPosX > gItem->boundingRect().width()) {
                        newPosX = gItem->boundingRect().width();
                    }

                    point->setPos(QPointF(std::abs(newPosX), point->pos().y()));
                }
            }
        }
    }

    scaleX->setScaleMaximum(value);
    scaleHorizontal->setScaleMaximum(value);
    scaleX->update();
    gItem->update();
    scene()->update();
}

void PlotGraphicsView::setOffset(int value)
{
    qDebug() << "setOffset" << value;

    scaleHorizontal->setOffset(value);
    scaleHorizontal->update();
}

void PlotGraphicsView::setData(int priority, const QColor &color, const QString &name, QVector<QPointF> &data)
{
    qDebug() << "set data";

    QVector<PointItem*> pointVector;

    //_pointVector.clear();   // чистим список точек (т.к. там пустые указатели)
    _selectedItems.clear(); // чистим выделенные точки (т.к. там пустые указатели)
    _selectedPoint = nullptr;

    gItem->setSelectedPoint(_selectedPoint);

    // сортировка по x и y
    //qSort(_data.begin(), _data.end(), lessThan);

    // очистим сцену от старого графика

    /*
    QList<QGraphicsItem*> items = this->scene()->items(this->sceneRect());

    for(QGraphicsItem *item : items) {

        if(item->type() == PointItem::Type || item->type() == LineItem::Type) {

            this->scene()->removeItem(item);
            delete item;
        }
    }

    */

    // Рисуем график

    if(data.size()) {
        QPointF point = data.at(0);

        qreal xPlot = gItem->boundingRect().width() * point.x() / 100.0;
        qreal yPlot = gItem->boundingRect().width() * (100 - point.y()) / 100.0;

        //qDebug() << "percent x = " << percentScaleX << ", y = " << percentScaleY;

        if(xPlot >= gItem->boundingRect().width()) {
            // временное решение. При перерисовке смотрим, чтобы точка не заходила за край границы
            // так происходит при уменьшении значения шкалы
            xPlot = gItem->boundingRect().width();
        }

        PointItem *pointLeft = new PointItem(QPointF(xPlot, yPlot), gItem);
        pointLeft->setDisableX(true);
        pointLeft->setColor(color);
        pointLeft->setZValue(priority);
        gItem->setPointLeft(pointLeft);

        for(int i = 0; i < data.count(); i++) {
            pointVector.push_back(pointLeft);

            if(i + 1 < data.count()) {
                // если есть следующая точка
                QPointF point = data.at(i + 1);

                qreal xPlot = gItem->boundingRect().width() * point.x() / 100.0;
                qreal yPlot = gItem->boundingRect().width() * (100 - point.y()) / 100.0;

                if(xPlot > gItem->boundingRect().width()) {
                    // временное решение. При перерисовке смотрим, чтобы точка не заходила за край границы
                    // так происходит при уменьшении значения шкалы
                    xPlot = gItem->boundingRect().width();
                }

                PointItem *pointRight = new PointItem(QPointF(xPlot, yPlot), gItem);
                pointRight->setColor(color);
                pointRight->setZValue(priority);

                LineItem * lineItem = new LineItem(pointLeft, pointRight, gItem);
                lineItem->setColor(color);
                lineItem->setZValue(priority);

                pointLeft->setLineRight(lineItem);
                pointRight->setLineLeft(lineItem);

                pointLeft = pointRight;
            }
            else {
                pointLeft->setDisableX(true);
                pointLeft->setPos(gItem->boundingRect().width(), pointLeft->pos().y());
                gItem->setPointRight(pointLeft);
            }
        }
    }
    else {
        // если нет данных для графика
        PointItem *pointLeft = new PointItem(QPointF(0, 0), gItem);
        pointLeft->setColor(color);
        pointLeft->setDisableX(true);
        pointLeft->setZValue(priority);

        PointItem *pointRight = new PointItem(QPointF(gItem->boundingRect().width(), 0), gItem);
        pointRight->setColor(color);
        pointRight->setDisableX(true);
        pointRight->setZValue(priority);

        LineItem * lineItem = new LineItem(pointLeft, pointRight, gItem);
        lineItem->setColor(color);
        lineItem->setZValue(priority);

        pointLeft->setLineRight(lineItem);
        pointRight->setLineLeft(lineItem);

        gItem->setPointLeft(pointLeft);
        gItem->setPointRight(pointRight);

        pointVector.push_back(pointLeft);
        pointVector.push_back(pointRight);

    }

    Channel *channel = new Channel(0, name, color, pointVector);
    _channelMap.insert(name, channel);

    this->scene()->update();
    gItem->update();

    if(_plotType == PlotType::PLOT_RGB)
        onPointChanged();

    // возвращаем координаты выбранной точки
    emit selectedPoint(-1, -1);

}

QVector<QPointF>& PlotGraphicsView::getData(const QString& name)
{
    _points.clear();
    //_pointVector.clear();

    Channel *channel = _channelMap.value(name);

    if(channel != nullptr) {
        //QVector<QPointF> points;
        for(PointItem* point : channel->_points) {

            qreal percentPlotX = point->pos().x() * 100.0 / gItem->boundingRect().width();
            //qreal xScale =  scaleX->getScaleMaximum() * percentPlotX / 100.0;
            qreal percentPlotY =  point->pos().y() * 100.0 / gItem->boundingRect().height();
            //qreal yScale =  scaleY->getScaleMaximum() * percentPlotY / 100.0;
            //qreal x = point->pos().x();
            //qreal y = point->pos().y();

            _points.push_back(QPointF(percentPlotX, 100 - percentPlotY));
        }
       // _points.insert(channel->_name, points);
       // сортировка по x и y
       //qSort(_data.begin(), _data.end(), lessThan);
    }

    return _points;
}

QList<QString> PlotGraphicsView::getPlotNames()
{
    return _channelMap.keys();
}

QColor PlotGraphicsView::getColorByName(const QString& name)
{
    QColor color(0, 0, 0);

    Channel *channel = _channelMap.value(name);

    if(channel != nullptr) {
        color = channel->_color;
    }

    return color;
}

bool PlotGraphicsView::getSelectedData()
{
    _dataSelected.clear();

    // собираем все выделенные точки по всем каналам (идут слева направо)
    for(QString& key : _channelMap.keys()) {
        Channel* channel = _channelMap.value(key);

        if(channel != nullptr) {
            QVector<QPointF> points;

            for(PointItem* point : channel->_points) {
                if(point->isSelect()) {
                    // если точка выделена
                    qreal percentPlotX = point->pos().x() * 100.0 / gItem->boundingRect().width();
                    qreal percentPlotY =  point->pos().y() * 100.0 / gItem->boundingRect().height();

                    points.push_back(QPointF(percentPlotX, percentPlotY));
                }
            }

            if(points.isEmpty() == false) {
                // если в канале есть выделенные точки
                _dataSelected.insert(key, QPair<QColor, QVector<QPointF> >(channel->_color, points));
            }
        }
    }

    return _dataSelected.isEmpty() == false;
}

void PlotGraphicsView::clear()
{
    // очистим сцену от старого графика
    for(auto &item : _selectedItems) {
        ((PointItem*)item)->select(false);
    }
    _selectedItems.clear();

    _selectedPoint = nullptr;

    gItem->setSelectedPoint(_selectedPoint);

    _selectedPoint = nullptr;

    gItem->setSelectedPoint(_selectedPoint);

    QList<QGraphicsItem*> items = this->scene()->items(this->sceneRect());

    for(QGraphicsItem *item : items) {
        if(item->type() == PointItem::Type || item->type() == LineItem::Type) {
            this->scene()->removeItem(item);
            delete item;
        }
    }

    for(Channel *channel: _channelMap.values()) {
        channel->_points.clear();
        delete channel;
    }

    _channelMap.clear();

    if(_plotType == PlotType::PLOT_RGB)
        onPointChanged();
}

bool PlotGraphicsView::isCursorCatched()
{
    return _isCursorCatched;
}

void PlotGraphicsView::setPosForSelectedPoint(double x, double y)
{
    // приводим к размеру сцены
    qreal percentScaleX = x * 100.0 / scaleX->getScaleMaximum();
    qreal xPlot = gItem->boundingRect().width() * percentScaleX / 100.0;

    qreal percentScaleY = y * 100.0 / scaleY->getScaleMaximum();
    qreal yPlot = gItem->boundingRect().height() * (100 - percentScaleY) / 100.0;


    if(_selectedItems.isEmpty() == false && _selectedPoint != nullptr) {
        QPointF newPoint(xPlot, yPlot);
        QPointF diffPoint = newPoint - _selectedPoint->pos(); // Разница между старой позицией и новой позицией точки, которая находится под курсором

        // Проверка, согласны ли остальные точки переместиться на указанной расстояние от своего нынешнего положения
        for(auto &item : _selectedItems) {

            PointItem *point = (PointItem*)item;
            QPointF posNew = point->pos() + diffPoint; // новая позиция точки

            checkPointForBorders(posNew, point);  // проверка выхода за границы
            checkPointForNeighbor(posNew, point); // проверка выхода за пределы соседних точек

            diffPoint = posNew - point->pos(); // расстояние, на которе можно переместить точки, с учетом возможностей всех точек на перемещение

        }

        for(auto &point : _selectedItems) {
            // перемещаем точки
            point->setPos(point->pos() + diffPoint);
        }

        xPlot = _selectedPoint->pos().x();
        yPlot = _selectedPoint->pos().y();

    }

    // приводим к размеру шкалы
    qreal percentPlotXNew = xPlot * 100.0 / gItem->boundingRect().width();
    qreal xScaleNew =  scaleX->getScaleMaximum() * percentPlotXNew / 100.0;

    qreal percentPlotYNew = 100 - yPlot * 100.0 / gItem->boundingRect().height();
    qreal yScaleNew =  scaleY->getScaleMaximum() * percentPlotYNew / 100.0;

    if(std::round(x) != std::round(xScaleNew) || std::round(y) != std::round(yScaleNew)) {
        // сигнал будет отсылаться только в том случае, если требуется поправка позиции
        // (поправка требуется когда возвращаемая позиция точки значительно отличающейся от задаваемой)
        emit selectedPoint(xScaleNew, yScaleNew);
    }

    this->scene()->update();

    if(_plotType == PlotType::PLOT_RGB)
        onPointChanged();

    //emit store(); // for undo-stack
}

void PlotGraphicsView::onPasteCancel()
{
    for(const QString& key: _pasteChannelMap.keys()) {
        for(PointItem *point : _pasteChannelMap.value(key)) {
            LineItem* lineLeft = (LineItem*)point->lineLeft();

            if(lineLeft != nullptr) {
                lineLeft->clearLinks();
                delete lineLeft;
            }

            LineItem* lineRight = (LineItem*)point->lineRight();

            if(lineRight != nullptr) {
                lineRight->clearLinks();
                delete lineRight;
            }

            //this->scene()->removeItem(point);
            delete point;
        }
    }

    _pasteChannelMap.clear();

    _pasteAreaItem->hide();

    //void pasteDone();
}

void PlotGraphicsView::onPasteAccept()
{
    for(const QString& key: _pasteChannelMap.keys()) {
         QVector<PointItem*> points = _pasteChannelMap.value(key);

         if(_isPasteReplace && points.count()) {
             // очистка области графика от точек
             QPointF pos = points.at(0)->pos();
             QPointF posItem = gItem->pos();
             QPointF posArea = _pasteAreaItem->pos();

             QPointF pointLeftInScene = _pasteAreaItem->mapToScene(points.at(0)->pos());
             QPointF pointLeft = gItem->mapFromScene(pointLeftInScene);                   // первая точка из вставляемой области

             QPointF pointRightInScene = _pasteAreaItem->mapToScene(points.at(points.count() - 1)->pos());
             QPointF pointRight = gItem->mapFromScene(pointRightInScene);                 // последняя точка из вставляемой области

             Channel* channel = _channelMap.value(key);

             if(channel != nullptr) {

                 for(int i = 0; i < channel->_points.count(); i++) {

                     PointItem *point = channel->_points.at(i);

                     if(point->pos().x() >= pointLeft.x() && point->pos().x() <= pointRight.x() && point->isDisableX() == false) {
                         // если точка на графике находится в пределах копируемой области, то удаляем ее
                         // за ичключением крайних точек, у которых отключена возможность перемещения по оси X

                         LineItem* lineLeft = static_cast<LineItem*>(point->lineLeft());
                         LineItem* lineRight = static_cast<LineItem*>(point->lineRight());

                         PointItem* pointLeftForLink = lineLeft->pointLeft();
                         PointItem* pointRightForLink = lineRight->pointRight();

                         LineItem* newLine = new LineItem(pointLeftForLink, pointRightForLink, gItem);
                         pointLeftForLink->setLineRight(newLine);
                         pointRightForLink->setLineLeft(newLine);

                         this->scene()->removeItem(lineLeft);
                         delete lineLeft;

                         this->scene()->removeItem(lineRight);
                         delete lineRight;

                         channel->_points.removeOne(point);

                         this->scene()->removeItem(point);
                         delete point;

                         i = 0;
                     }
                 }
             }
         }

         for(PointItem* pastePoint : points) {
            // проходимся по всем копируемым токам и встаяляем их
            QPointF pointInScene = _pasteAreaItem->mapToScene(pastePoint->pos());
            QPointF pointToPlot = gItem->mapFromScene(pointInScene);

            pointToPlot = QPointF(QString::number(pointToPlot.x(), 'f', 4).toDouble(), pointToPlot.y()); // Округление до 4 знаков после запятой !!!

            qDebug() << "pasted point = " <<  pointToPlot;

            // поиск канала и точек, между которыми находится участок графика, который мы хотим вставить
            Channel* channel = _channelMap.value(key);

            if(channel != nullptr) {

                for(int i = 1; i < channel->_points.count(); i++) {

                    PointItem* pointLeft = channel->_points.at(i - 1);
                    PointItem* pointRight = channel->_points.at(i);

                    qDebug() << "left point = " << pointLeft->pos() << ", right point =" << pointRight->pos();

                    if(pointToPlot.x() >= pointLeft->pos().x() && pointToPlot.x() <= pointRight->pos().x()) {

                        if(//_isPasteReplace == false
                                pointToPlot.x() == pointRight->pos().x()
                                && pointRight->isDisableX() == false) {
                            // если вставляемая точка имеет такое же время как и правая граница,
                            // при условии, что это не крайняя правая точка
                            // то идем к следующему отрезку

                            continue;
                        }

                        LineItem* line = static_cast<LineItem*>(pointLeft->lineRight());

                        this->scene()->removeItem(line);

                        delete line;

                        PointItem *newPoint = new PointItem(pointToPlot, gItem);
                        newPoint->select(true);
                        newPoint->setColor(getColorByName(key));


                        _selectedItems.insert(newPoint);

                        LineItem *lineLeft = new LineItem(pointLeft, newPoint, gItem);
                        lineLeft->setColor(getColorByName(key));

                        LineItem *lineRight = new LineItem(newPoint, pointRight, gItem);
                        lineRight->setColor(getColorByName(key));

                        pointLeft->setLineRight(lineLeft);
                        pointRight->setLineLeft(lineRight);

                        newPoint->setLineLeft(lineLeft);
                        newPoint->setLineRight(lineRight);

                        channel->_points.insert(i, newPoint);

                        if(_isPasteReplace && points.indexOf(pastePoint) == 0
                                && pointToPlot.x() == pointLeft->pos().x() && pointLeft->isDisableX()) {
                            // если режим замены и если первая точка,
                            // и координаты которой совпадают с крайней левой точкой канала

                            newPoint->setDisableX(true);


                            LineItem* lineLeft = static_cast<LineItem*>(newPoint->lineLeft());
                            this->scene()->removeItem(lineLeft);
                            delete lineLeft;

                            channel->_points.removeOne(pointLeft);
                            this->scene()->removeItem(pointLeft);
                            delete pointLeft;

                            newPoint->setLineLeft(nullptr);


                        }

                        if(_isPasteReplace && points.indexOf(pastePoint) == points.count() - 1
                                && pointToPlot.x() == pointRight->pos().x() && pointRight->isDisableX()) {
                            // если режим замены и если последняя точка,
                            // и координаты которой совпадают с крайней левой точкой канала

                            newPoint->setDisableX(true);


                            LineItem* lineRight = static_cast<LineItem*>(newPoint->lineRight());
                            this->scene()->removeItem(lineRight);
                            delete lineRight;

                            channel->_points.removeOne(pointRight);
                            this->scene()->removeItem(pointRight);
                            delete pointRight;

                            newPoint->setLineRight(nullptr);

                        }

                        break;

                    }
                }
             }
         }
    }

    onPasteCancel();

    emit store(); // для undo


    if(_plotType == PlotType::PLOT_RGB)
        onPointChanged();

    emit pasteDone();

    this->setFocus();


}

void PlotGraphicsView::onPasteMirror(MirrorType type)
{
    if(type == MirrorType::MirrorHorizontal) {
        // отображение по горизонтали

        for(QString& channel : _pasteChannelMap.keys()) {
            // проходим по каналам

            QVector<PointItem*> points = _pasteChannelMap.value(channel);

            for(PointItem* point : points) {
                // проходим по точкам

                point->setPos(QPointF(_pasteAreaItem->rect().width() - point->pos().x(), point->pos().y()));

            }

            // разворачиваем вектор с точками в противоположную сторону
            std::reverse(points.begin(), points.end());
            _pasteChannelMap.insert(channel, points);
        }
    }
    else if(type == MirrorType::MirrorVertical){
        // отображение по вертикали

        for(QString& channel : _pasteChannelMap.keys()) {
            // проходим по каналам

            QVector<PointItem*> points = _pasteChannelMap.value(channel);

            for(PointItem* point : points) {
                // проходим по точкам
                point->setPos(QPointF(point->pos().x(), _pasteAreaItem->rect().height() - point->pos().y()));

            }

        }
    }

}

void PlotGraphicsView::onMergePastedChannels(const QVector<QPair<QString, QString> > &settingsChannel)
{
    QMap<QString, QVector<PointItem*> > pasteChannelMapCopy;

    for(const QPair<QString, QString>& pair : settingsChannel) {
        // меняем цвет у графиков
        QVector<PointItem*> points = _pasteChannelMap.value(pair.first);

        if(pasteChannelMapCopy.contains(pair.second)) {
            // если уже есть график с таким ключем, то сливаем их

            QVector<PointItem*> pastedPoints = pasteChannelMapCopy.value(pair.second); // точки с которыми будем сливать

            // сливаем точки
            for(PointItem* withPoint : points) {

                for(int i = pastedPoints.count() - 1 ; i >= 0; i--) {

                    PointItem* pastedPointLeft = pastedPoints.at(i);

                    if(withPoint->pos().x() >= pastedPointLeft->pos().x()) {
                        // если точка по Х больше, то вставляем ее
                        pastedPoints.insert(i + 1, withPoint);

                        break;
                    }
                    else if(i == 0) {
                        // если позиция вставляемой точки самая крайняя, то вставляем ее в начало
                        pastedPoints.insert(0, withPoint);
                    }

                }

            }

            pasteChannelMapCopy.insert(pair.second, pastedPoints);

        }
        else {
            // если нет такого графика, то добавляем
            pasteChannelMapCopy.insert(pair.second, points);
        }
    }


    _pasteChannelMap.clear();
    _pasteChannelMap = pasteChannelMapCopy;

    // раскрашиваем точки и линии под цвет канала
    // (нужно совмещать графики!!!)
    for(const QString& key : _pasteChannelMap.keys()) {

        qDebug() << "pasted channel" << key;

        for(PointItem *point : _pasteChannelMap.value(key)) {

            point->setColor(getColorByName(key));

            LineItem* lineLeft = static_cast<LineItem*>(point->lineLeft());

            if(lineLeft != nullptr) {
                lineLeft->setColor(getColorByName(key));
            }

            LineItem* lineRight = static_cast<LineItem*>(point->lineRight());

            if(lineRight != nullptr) {
                lineRight->setColor(getColorByName(key));
            }
        }

    }

    _pasteAreaItem->update();


    emit pastePrepared(_pasteChannelMap.keys(), _channelMap.keys());

}

void PlotGraphicsView::wheelEvent(QWheelEvent * event)
{
    QPointF cursorPosOld = QPointF(0, 0);
    if(_cursorItem != nullptr) {
        cursorPosOld = gItem->mapFromScene(_cursorItem->pos());

    }

    if (event->modifiers() & Qt::AltModifier) {

        // Масштабирование по оси X

        const QPointF p0scene = gItem->mapFromScene(mapToScene(event->pos()));

        QTransform transform = gItem->transform();

        // Проверяем, чтобы фактор масштабирование не был меньше фактора, который мы задаем при растягиваниии окна
        qreal scaleFactorX = transform.m11() + (event->delta() / std::abs(event->delta()) * SCALE_FACTOR);

        if(scaleFactorX < _scaleFactorX) {
            scaleFactorX = _scaleFactorX;
        }

        QMatrix matrix;
        matrix.scale(scaleFactorX, transform.m22());

        gItem->setMatrix(matrix);

        this->scene()->setSceneRect(QRectF(0, 0, gItem->sceneBoundingRect().width() + LEFT_FILED_WIDTH + RIGHT_FILED_WIDTH,
                                           gItem->sceneBoundingRect().height() + LEFT_FILED_WIDTH + LEFT_FILED_WIDTH));


        const QPointF p1mouse = gItem->mapToScene(p0scene);
        const QPointF move = p1mouse - mapToScene(event->pos()); // The move

        horizontalScrollBar()->setValue(move.x() + horizontalScrollBar()->value());
        verticalScrollBar()->setValue(move.y() + verticalScrollBar()->value());

    }
    else if(event->modifiers() & Qt::ShiftModifier) {

        // Масштабирование по оси Y

        /*
        if(verticalScrollBar()->maximum() <= 0 && event->delta() < 0) {
            return;
        }
        */

        const QPointF p0scene = gItem->mapFromScene(mapToScene(event->pos()));

        QTransform transform = gItem->transform();        

        // Проверяем, чтобы фактор масштабирование не был меньше фактора, который мы задаем при растягиваниии окна
        qreal scaleFactorY = transform.m22() + (event->delta() / std::abs(event->delta()) * SCALE_FACTOR);

        if(scaleFactorY < _scaleFactorY) {
            scaleFactorY = _scaleFactorY;
        }

        QMatrix matrix;
        matrix.scale(transform.m11(), scaleFactorY);

        gItem->setMatrix(matrix);

        this->scene()->setSceneRect(QRectF(0, 0, gItem->sceneBoundingRect().width() + LEFT_FILED_WIDTH + RIGHT_FILED_WIDTH,
                                           gItem->sceneBoundingRect().height() + LEFT_FILED_WIDTH + LEFT_FILED_WIDTH ));

        const QPointF p1mouse = gItem->mapToScene(p0scene);
        const QPointF move = p1mouse - mapToScene(event->pos()); // The move

        horizontalScrollBar()->setValue(move.x() + horizontalScrollBar()->value());
        verticalScrollBar()->setValue(move.y() + verticalScrollBar()->value());

    }
    else /*if(event->modifiers() & Qt::ControlModifier)*/ {

        // Масштабирование по двум осям

        /*
        if(horizontalScrollBar()->maximum() <= 0 && event->delta() < 0) {
            return;
        }
        */

        const QPointF p0scene = gItem->mapFromScene(mapToScene(event->pos()));

        QTransform transform = gItem->transform();

        // Проверяем, чтобы фактор масштабирование не был меньше фактора, который мы задаем при растягиваниии окна
        qreal scaleFactorX = transform.m11() + (event->delta() / std::abs(event->delta()) * SCALE_FACTOR);

        if(scaleFactorX < _scaleFactorX) {
            scaleFactorX = _scaleFactorX;
        }

        qreal scaleFactorY = transform.m22() + (event->delta() / std::abs(event->delta()) * SCALE_FACTOR);

        if(scaleFactorY < _scaleFactorY) {

            scaleFactorY = _scaleFactorY;
        }

        QMatrix matrix;
        matrix.scale(scaleFactorX, scaleFactorY);

        gItem->setMatrix(matrix);

        this->scene()->setSceneRect(QRectF(0, 0, gItem->sceneBoundingRect().width() + LEFT_FILED_WIDTH + RIGHT_FILED_WIDTH,
                                           gItem->sceneBoundingRect().height() + LEFT_FILED_WIDTH + LEFT_FILED_WIDTH));


        const QPointF p1mouse = gItem->mapToScene(p0scene);
        const QPointF move = p1mouse - mapToScene(event->pos()); // The move

        horizontalScrollBar()->setValue(move.x() + horizontalScrollBar()->value());
        verticalScrollBar()->setValue(move.y() + verticalScrollBar()->value());

    }
//    else {
//        QGraphicsView::wheelEvent(event);
//    }

    // repaint scales position

    scaleX->setRect(QRectF(QPoint(0, this->sceneRect().height() - verticalScrollBar()->maximum() - LEFT_FILED_WIDTH),
                           QSizeF(this->sceneRect().width() - horizontalScrollBar()->maximum(), LEFT_FILED_WIDTH)));
    scaleX->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    scaleX->setScaleFactor(gItem->matrix().m11());

    scaleY->setRect(QRectF(QPointF(0, 0), QSizeF(LEFT_FILED_WIDTH, this->sceneRect().height() - verticalScrollBar()->maximum())));
    scaleY->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    scaleY->setScaleFactor(gItem->matrix().m22());


    scaleHorizontal->setRect(QRectF(QPoint(LEFT_FILED_WIDTH, 0),
                           QSizeF(this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH, LEFT_FILED_WIDTH)));
    scaleHorizontal->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    scaleVertical->setRect(QRectF(QPointF(this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH, 0),
                           QSizeF(LEFT_FILED_WIDTH, this->sceneRect().height() - verticalScrollBar()->maximum())));
    scaleVertical->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    if(_cursorItem != nullptr) {

        _cursorItem->setRect(QRectF(QPointF(0, 0), QSizeF(LEFT_FILED_WIDTH * 0.5, this->sceneRect().height() - verticalScrollBar()->maximum())));

        QPointF cursorPosNew = gItem->mapToScene(cursorPosOld);

        _cursorItem->setPos(QPointF(cursorPosNew.x(), verticalScrollBar()->value()));

    }

    this->scene()->update();

}

void PlotGraphicsView::resizeEvent(QResizeEvent * event)
{
    QPointF cursorPosOld = QPointF(0, 0);
    if(_cursorItem != nullptr) {
        cursorPosOld = gItem->mapFromScene(_cursorItem->pos());

    }

    // При именении размеров окна масштабируем сцену, так, чтобы она совпала с новыми размерами окна
     QMatrix matrix = gItem->matrix();
     matrix.scale(this->transform().m11() / _scaleFactorX, this->transform().m22() / _scaleFactorY);

     QRectF viewRect =  mapToScene(this->viewport()->rect()).boundingRect(); // viewport на сцене

     _scaleFactorX = (viewRect.width() - (LEFT_FILED_WIDTH + RIGHT_FILED_WIDTH)) / gItem->boundingRect().width();
     _scaleFactorY = (viewRect.height() - (LEFT_FILED_WIDTH + LEFT_FILED_WIDTH)) / gItem->boundingRect().height();
    // нельзя уменьшать картинку
     //_scaleFactorX = _scaleFactorX < 1 ? 1 : _scaleFactorX;
     //_scaleFactorY = _scaleFactorY < 1 ? 1 : _scaleFactorY;

     matrix.scale(_scaleFactorX, _scaleFactorY);

     gItem->setMatrix(matrix);
     gItem->setPos(QPointF(LEFT_FILED_WIDTH, LEFT_FILED_WIDTH));

     this->scene()->setSceneRect(QRectF(0, 0,
                                        gItem->sceneBoundingRect().width() + LEFT_FILED_WIDTH + RIGHT_FILED_WIDTH,
                                        gItem->sceneBoundingRect().height() + LEFT_FILED_WIDTH + LEFT_FILED_WIDTH));


      // repaint scales position

     scaleX->setRect(QRectF(QPoint(0, this->sceneRect().height() - verticalScrollBar()->maximum() - LEFT_FILED_WIDTH),
                            QSizeF(this->sceneRect().width() - horizontalScrollBar()->maximum(), LEFT_FILED_WIDTH)));
     scaleX->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

     scaleX->setScaleFactor(gItem->matrix().m11());

     scaleY->setRect(QRectF(QPointF(0, 0), QSizeF(LEFT_FILED_WIDTH, this->sceneRect().height() - verticalScrollBar()->maximum())));
     scaleY->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

     scaleY->setScaleFactor(gItem->matrix().m22());

     scaleHorizontal->setRect(QRectF(QPoint(LEFT_FILED_WIDTH, 0),
                            QSizeF(this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH, LEFT_FILED_WIDTH)));
     scaleHorizontal->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

     scaleVertical->setRect(QRectF(QPointF(this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH, 0),
                            QSizeF(LEFT_FILED_WIDTH, this->sceneRect().height() - verticalScrollBar()->maximum())));
     scaleVertical->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

     if(_cursorItem != nullptr) {



         _cursorItem->setRect(QRectF(QPointF(0, 0), QSizeF(LEFT_FILED_WIDTH * 0.5, this->sceneRect().height() - verticalScrollBar()->maximum())));

         QPointF cursorPosNew = gItem->mapToScene(cursorPosOld);

         _cursorItem->setPos(QPointF(cursorPosNew.x(), verticalScrollBar()->value()));

     }

     this->scene()->update();

}

void PlotGraphicsView::onScrollBarChanged(int val)
{

    QPointF cursorPosOld = QPointF(0, 0);
    if(_cursorItem != nullptr) {
        cursorPosOld = gItem->mapFromScene(_cursorItem->pos());

    }

    // repaint scales position
    scaleX->setRect(QRectF(QPoint(0, this->sceneRect().height() - verticalScrollBar()->maximum() - LEFT_FILED_WIDTH),
                           QSizeF(this->sceneRect().width() - horizontalScrollBar()->maximum(), LEFT_FILED_WIDTH)));
    scaleX->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    scaleX->setScaleFactor(gItem->matrix().m11());

    scaleY->setRect(QRectF(QPointF(0, 0), QSizeF(LEFT_FILED_WIDTH, this->sceneRect().height() - verticalScrollBar()->maximum())));
    scaleY->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    scaleY->setScaleFactor(gItem->matrix().m22());

    scaleHorizontal->setRect(QRectF(QPoint(LEFT_FILED_WIDTH, 0),
                           QSizeF(this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH, LEFT_FILED_WIDTH)));
    scaleHorizontal->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    scaleVertical->setRect(QRectF(QPointF(this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH, 0),
                           QSizeF(LEFT_FILED_WIDTH, this->sceneRect().height() - verticalScrollBar()->maximum())));
    scaleVertical->setPos(QPointF(horizontalScrollBar()->value(), verticalScrollBar()->value()));

    if(_cursorItem != nullptr) {

        _cursorItem->setRect(QRectF(QPointF(0, 0), QSizeF(LEFT_FILED_WIDTH * 0.5, this->sceneRect().height() - verticalScrollBar()->maximum())));

        QPointF cursorPosNew = gItem->mapToScene(cursorPosOld);

        _cursorItem->setPos(QPointF(cursorPosNew.x(), verticalScrollBar()->value()));

    }

    this->scene()->update();


    qreal width = this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH;
    qreal height = this->sceneRect().height() - verticalScrollBar()->maximum() - LEFT_FILED_WIDTH;

    //this->mapToScene(QRectF(40, 40, width, height));

    qDebug() << "visible top" << gItem->mapFromScene(QPointF(scaleX->pos().x() + LEFT_FILED_WIDTH, scaleX->pos().y() + LEFT_FILED_WIDTH));
    qDebug() << "visible bottom" << gItem->mapFromScene(QPointF(scaleX->pos().x() + width, scaleX->pos().y() + height));

}

void PlotGraphicsView::onPointChanged()
{
    QVector<PointItem*> generalPoints;

    QVector<QPair<double, QColor> > points;

    for(int i = 0; i < gItem->boundingRect().width(); i++) {

        QColor color;
        int r = 0;
        int g = 0;
        int b = 0;

        for (Channel *channel: _channelMap.values()) {
            // проход по всем каналам
            if(channel->_points.count()) {
                QPointF pointLeft = channel->_points.at(0)->pos();
                for(int j = 1; j < channel->_points.count(); j++) {
                    // проход по точкам канала
                    QPointF pointRight = channel->_points.at(j)->pos();

                    if(i >= pointLeft.x() && i <= pointRight.x()) {

                        qreal x_min = (( pointRight.x() <= pointLeft.x() ) ? pointRight.x() : pointLeft.x());
                        qreal x_max = (( pointRight.x() >= pointLeft.x() ) ? pointRight.x() : pointLeft.x());
                        qreal y_min = (( pointRight.y() <= pointLeft.y() ) ? pointRight.y() : pointLeft.y());
                        qreal y_max = (( pointRight.y() >= pointLeft.y() ) ? pointRight.y() : pointLeft.y());

                        int direction = 0;

                        if (pointLeft.y() != pointRight.y()) {

                          direction = ((pointLeft.y() < pointRight.y()) ? 1 : -1);
                        }

                        qreal delta_x = x_max - x_min;
                        qreal delta_y = y_max - y_min;

                        qreal y = 0;
                        if(direction == -1) {

                            if(delta_x > 0) {
                                y = std::abs(y_min + delta_y - (delta_y / delta_x) * (i - x_min));
                            }
                            else {
                                y = std::abs(y_min + delta_y);
                            }
                        }
                        else {
                            if(delta_x > 0) {
                                y =  std::abs(y_min + (delta_y / delta_x) * (i - x_min));
                            }
                            else {
                                y =  std::abs(y_min);
                            }
                        }

                        if(channel->_name == "R") {
                            //color.setRed(255 * y / 100);
                            y =  y * 100 / PLOT_WIDTH;

                            r = (r + 255 * (100 - y) / 100);
                            r = (r > 255) ? 255 : r;
                        }
                        else if(channel->_name == "G") {
                            //color.setGreen(255 * y / 100);
                            y =  y * 100 / PLOT_WIDTH;

                            g = (g + 255 * (100 - y) / 100);
                            g = (g > 255) ? 255 : g;
                        }
                        else if(channel->_name == "B") {
                            //color.setBlue(255 * y / 100);
                            y =  y * 100 / PLOT_WIDTH;

                            b = (b + 255 * (100 - y) / 100);
                            b = (b > 255) ? 255 : b;

                        }
                        else if(channel->_name == "W") {
                            y =  y * 100 / PLOT_WIDTH;

                            r = (r + 255 * (100 - y) / 100);
                            r = (r > 255) ? 255 : r;

                            g = (g + 255 * (100 - y) / 100);
                            g = (g > 255) ? 255 : g;

                            b = (b + 255 * (100 - y) / 100);
                            b = (b > 255) ? 255 : b;

                        }

                    }

                    pointLeft = pointRight;

                }
            }
        }

        points.push_back(QPair<double, QColor>(i, QColor(r, g, b)));
        //qDebug() << "p = " << i << "color = " << r << g << b;

    }

    scaleX->setGradient(points);
}

void PlotGraphicsView::onCurrentColorChanged(const QVector<QPair<QString, int>>& channel_states)
{

    // проходим по всем каналам, которые прислал виджет
    for(const QPair<QString, int>& state: channel_states) {

        if(channels_map.contains(state.first)) {
            // если такой канал есть среди каналов, которые мы отслеживаем на изменение

            if(channels_map.value(state.first) != state.second) {
                // если уровень канала изменился

                qDebug() << "channel = " << state.first << state.second;

                channels_map.insert(state.first, state.second); // применяем новое занчение к каналу

                // проверяем, если ли среди выделенных точка из этого канала
                QSet<QGraphicsItem*>::iterator iter = std::find_if(_selectedItems.begin(),
                                                                   _selectedItems.end(),
                                                                   [&] (QGraphicsItem* item) {
                        Channel* channel = _channelMap.value(state.first);

                        return channel->_points.contains(static_cast<PointItem*>(item));

                });

                if(iter != _selectedItems.end()) {
                    //если среди выделенных точек уже есть точка с таким же цветом
                    // то перемещаем ее на нужный уровень

                    PointItem* pointItem = (PointItem*)(*iter);

                    QPointF pos = pointItem->pos();

                    qreal yPercent = state.second * 100 / scaleY->getScaleMaximum();
                    qreal yReal = (100 - yPercent) * gItem->boundingRect().height() / 100;

                    pointItem->setPos(pos.x(), yReal);
                    gItem->setSelectedPoint(pointItem);

                }
                else {
                    //если среди выделенных точек нет точки с таким цветом
                    // то создаем новую точку

                    Channel* channel = _channelMap.value(state.first);

                    if(channel != nullptr) {
                        // проходим по точкам канала, и найдем место, куда вставим новую точку
                        for(int i = 1; i < channel->_points.count(); i++) {
                            PointItem* pointLeft = channel->_points.at(i - 1);
                            PointItem* pointRight = channel->_points.at(i);

                            if(_sceneToPlotPoint.x() >= pointLeft->pos().x() && _sceneToPlotPoint.x() <= pointRight->pos().x()) {
                                qreal y = lightcoder::ScriptPanel::GetPointPojection(_sceneToPlotPoint, pointLeft->pos(), pointRight->pos());

                                qreal yPercent = 100 - (y * 100 / gItem->boundingRect().height()); // позиция точки в процентах

                                channels_map.insert(state.first, scaleY->getScaleMaximum() * yPercent / 100);

                                // новая точка
                                QGraphicsLineItem* lineItem = pointLeft->lineRight();

                                this->scene()->removeItem(lineItem);

                                delete lineItem;

                                PointItem *pNew = new PointItem(QPointF(_sceneToPlotPoint.x(), y), gItem);
                                pNew->setColor(channel->_color);

                                LineItem *line1 = new LineItem(pointLeft, pNew, gItem);
                                line1->setColor(channel->_color);
                                line1->setZValue(channel->_id);
                                pointLeft->setLineRight(line1);

                                LineItem *line2 = new LineItem(pNew, pointRight, gItem);
                                line2->setColor(channel->_color);
                                line2->setZValue(channel->_id);
                                pointRight->setLineLeft(line2);

                                pNew->setLineLeft(line1);
                                pNew->setLineRight(line2);

                                // втсавка новой точки между точками, которые были концами линии
                                auto iterator = std::find_if(channel->_points.begin(), channel->_points.end(), [&](PointItem *p) {
                                    return p->pos().x() == pointRight->x() && p->pos().y() == pointRight->y();
                                });

                                channel->_points.insert(iterator, pNew);

                                _selectedItems.insert(pNew); // добавим точку в список выделенных
                                pNew->select(true);          // и подсветим ее как выделенную

                                _selectedPoint = pNew;
                                gItem->setSelectedPoint(_selectedPoint);

                                break;
                            }
                        }
                    }
                }
            }
        }

    }

    this->scene()->update();

    if(_plotType == PlotType::PLOT_RGB)
        onPointChanged();
}

/*
void PlotGraphicsView::onTimeout()
{    
    if(_isCursorCatched) {
        // если курсор зажат курсором, то ничего не делаем
        return;
    }

    //getCursorPosition(true);

    //emit Ticked(100);


}
*/



void PlotGraphicsView::start()
{
    _cursorItem->show();
    //_timer->start();
}

void PlotGraphicsView::stop()
{
    //_timer->stop();
    _cursorItem->hide();
    _cursorItem->setPos(horizontalScrollBar()->value() + 40, verticalScrollBar()->value());
}

void PlotGraphicsView::pause()
{
    //_timer->stop();
}

PointItem* PlotGraphicsView::findRightSelcetedPoint(PointItem* from)
{
    PointItem *pointRight = from;

    while(pointRight != nullptr && _selectedItems.contains(pointRight)) {

        LineItem *lineRight = (LineItem*)pointRight->lineRight();

        if(lineRight != nullptr) {
            pointRight = lineRight->pointRight();
        }
        else {
            break;
        }
    }

    return pointRight;
}

PointItem* PlotGraphicsView::findLeftSelcetedPoint(PointItem* from)
{
    PointItem *pointLeft = from;

    while(pointLeft != nullptr && _selectedItems.contains(pointLeft)) {

        LineItem *lineLeft = (LineItem*)pointLeft->lineLeft();

        if(lineLeft != nullptr) {
            pointLeft = lineLeft->pointLeft();
        }
        else {
            break;
        }
    }

    return pointLeft;

}

PointItem* PlotGraphicsView::findTopSelectedPoint()
{
    PointItem *point = nullptr;

    for(auto &item: _selectedItems) {

        if(point == nullptr) {
            point = (PointItem*)item;
        }
        else {
            if(item->pos().y() < point->pos().y() ) {
                // < т.к Y считается от левого верхнего угла вниз
                point = (PointItem*)item;
            }
        }
    }

    return point;
}

PointItem* PlotGraphicsView::findBottomSelectedPoint()
{
    PointItem *point = nullptr;

    for(auto &item: _selectedItems) {

        if(point == nullptr) {
            point = (PointItem*)item;
        }
        else {
            if(item->pos().y() > point->pos().y() ) {
                // > т.к Y считается от левого верхнего угла вниз
                point = (PointItem*)item;
            }
        }
    }

    return point;
}

void PlotGraphicsView::checkPointForBorders(QPointF& pos, PointItem *point)
{

    // Проверяем, чтобы точка не выходила за пределы области gItem
    if(pos.x() > gItem->boundingRect().width()) {
        pos = QPointF(gItem->boundingRect().width(), pos.y());
    }
    else if(pos.x() < 0) {
        pos = QPointF(0, pos.y());
    }

    if(pos.y() > gItem->boundingRect().height()) {
        pos = QPointF(pos.x(), gItem->boundingRect().height());
    }
    else if(pos.y() < 0) {
        pos = QPointF(pos.x(), 0);
    }


    if(point->isDisableX()) {
        //  если запрещено изменять положение по x
        pos = QPointF(point->pos().x(), pos.y());
    }

    if(point->isDisableY()) {
        //  если запрещено изменять положение по x
        pos = QPointF(pos.x(), point->pos().y());
    }

}

QPointF PlotGraphicsView::checkPointForBorders2(QPointF point)
{
    qreal width = this->sceneRect().width() - horizontalScrollBar()->maximum() - LEFT_FILED_WIDTH;
    qreal height = this->sceneRect().height() - verticalScrollBar()->maximum() - LEFT_FILED_WIDTH;

    QPointF pointTop = gItem->mapFromScene(QPointF(scaleX->pos().x() + LEFT_FILED_WIDTH, scaleX->pos().y() + LEFT_FILED_WIDTH));
    QPointF pointBottom = gItem->mapFromScene(QPointF(scaleX->pos().x() + width, scaleX->pos().y() + height));

    // Проверяем, чтобы точка не выходила за пределы области gItem
    if(point.x() > pointBottom.x()) {
        point = QPointF(pointBottom.x(), point.y());
    }
    else if(point.x() < pointTop.x()) {
        point = QPointF(pointTop.x(), point.y());
    }

    if(point.y() > pointBottom.y()) {
        point = QPointF(point.x(), pointBottom.y());
    }
    else if(point.y() < pointTop.y()) {
        point = QPointF(point.x(), pointTop.y());
    }


    return point;
}

void PlotGraphicsView::checkPointForNeighbor(QPointF& pos, PointItem *point)
{

    // Проверяем, чтобы точка не выходила за границы слева расположенной от нее точки
    LineItem *lineRight = (LineItem*)point->lineRight();
    if(lineRight != nullptr) {
        PointItem *pRight = lineRight->pointRight();

        if(pRight != nullptr && pRight->isSelect() == false) {

            if(pos.x() > pRight->x()) {
                pos = QPointF(pRight->x(), pos.y());
            }

        }
    }

    // Проверяем, чтобы точка не выходила за границы слева расположенной от нее точки
    LineItem *lineLeft = (LineItem*)point->lineLeft();
    if(lineLeft != nullptr) {
        PointItem *pLeft = lineLeft->pointLeft();

        if(pLeft != nullptr && pLeft->isSelect() == false) {

            if(pos.x() < pLeft->x()) {
                pos = QPointF(pLeft->x(), pos.y());
            }

        }
    }


}

void PlotGraphicsView::setCursorForRect(const QPointF& pos, const QRectF& rect)
{
    if(pos.x() >= rect.x() - BORDER_OFFSIDE
            && pos.x() <= rect.x() + BORDER_OFFSIDE
            && pos.y() >= rect.y() - BORDER_OFFSIDE
            && pos.y() <= rect.y() + BORDER_OFFSIDE) {

        // left top
        setCursor(Qt::SizeFDiagCursor);

    }
    else if(pos.x() >= rect.x() + rect.width() - BORDER_OFFSIDE
             && pos.x() <= rect.x() + rect.width() + BORDER_OFFSIDE
             && pos.y() >= rect.y() - BORDER_OFFSIDE
             && pos.y() <= rect.y() + BORDER_OFFSIDE) {

         // right top
         setCursor(Qt::SizeBDiagCursor);

     }
     else if(pos.x() >= rect.x() - BORDER_OFFSIDE
             && pos.x() <= rect.x() + BORDER_OFFSIDE
             && pos.y() >= rect.y() + rect.height() - BORDER_OFFSIDE
             && pos.y() <= rect.y() + rect.height() + BORDER_OFFSIDE) {

         // left bottom
         setCursor(Qt::SizeBDiagCursor);

     }
     else if(pos.x() >= rect.x() + rect.width() - BORDER_OFFSIDE
             && pos.x() <= rect.x() + rect.width() + BORDER_OFFSIDE
             && pos.y() >= rect.y() + rect.height() - BORDER_OFFSIDE
             && pos.y() <= rect.y() + rect.height() + BORDER_OFFSIDE) {

         // right bottom
         setCursor(Qt::SizeFDiagCursor);

     }
     else if(pos.x() >= rect.x() + rect.width() - BORDER_OFFSIDE
             && pos.x() <= rect.x() + rect.width() + BORDER_OFFSIDE) {

         setCursor(Qt::SizeHorCursor);
     }
     else if(pos.y() >= rect.y() + rect.height() - BORDER_OFFSIDE
             && pos.y() <= rect.y() + rect.height() + BORDER_OFFSIDE) {

         setCursor(Qt::SizeVerCursor);
     }
     else if(pos.x() >= rect.x() - BORDER_OFFSIDE
             && pos.x() <= rect.x() + BORDER_OFFSIDE) {

         setCursor(Qt::SizeHorCursor);
     }
     else if(pos.y() >= rect.y() - BORDER_OFFSIDE
             && pos.y() <= rect.y() + BORDER_OFFSIDE) {

         setCursor(Qt::SizeVerCursor);
     }
     else if(pos.x() >= rect.x()
             && pos.x() <= rect.x() + rect.width()
             && pos.y() >= rect.y()
             && pos.y() <= rect.y() + rect.height()) {

        setCursor(Qt::SizeAllCursor);

     }
     else {
        this->setCursor(Qt::OpenHandCursor);
     }
}

qreal PlotGraphicsView::setCursorPosition(int ms)
{
    /*
    QPointF posCursorInPlot = gItem->mapFromScene(_cursorItem->pos());
    qreal posPercent = posCursorInPlot.x() * 100 / gItem->boundingRect().width();
    qreal tickPercent = ms * 100.0 / scaleX->getScaleMaximum(); // 5 секунд в процентах от общего времени

    qDebug() << "pos = " << posCursorInPlot << posPercent;

    posPercent += tickPercent;

    QPointF newPos;
    if(_cursorItem->pos().x() + 5 > sceneRect().width()) {
        newPos = QPointF(0, _cursorItem->pos().y());
    }
    else {
        newPos = QPointF(_cursorItem->pos().x() + 5, _cursorItem->pos().y());
    }

    if(posPercent >= 100) {
        posPercent = 0;
    }

    qreal pos = gItem->boundingRect().width() * posPercent / 100;

    QPointF pos2 = gItem->mapToScene(QPointF(pos, 0));

    _cursorItem->setPos(QPointF(pos2.x(), _cursorItem->pos().y()));

    return posPercent;
    */


    //emit Ticked(posPercent);

    int posInMs = getCursorPositionMs();

    posInMs += ms;

    if(posInMs > scaleX->getScaleMaximum()) {
        posInMs = 0;
    }

    qreal posInPercent = posInMs * 100.0 / scaleX->getScaleMaximum(); // позиция процентах от общего времени

    qreal pos = posInPercent * gItem->boundingRect().width() / 100;

    QPointF posCursorToPlot = gItem->mapToScene(QPointF(pos, _cursorItem->pos().y()));

    _cursorItem->setPos(QPointF(posCursorToPlot.x(), verticalScrollBar()->value()));

    return posInPercent;

}

int PlotGraphicsView::getCursorPositionMs()
{
    QPointF posCursorInPlot = gItem->mapFromScene(_cursorItem->pos());
    qreal posPercent = posCursorInPlot.x() * 100 / gItem->boundingRect().width();
    int ms = posPercent * scaleX->getScaleMaximum() / 100;

    return ms;
}

qreal PlotGraphicsView::getCursorPositionByMs(int ms)
{
    return ms * 100.0 / scaleX->getScaleMaximum(); // позиция процентах от общего времени
}

void PlotGraphicsView::copy()
{
    if(getSelectedData()) {
        // если есть выделенные точки

        QByteArray data;
        QDataStream in(&data, QIODevice::WriteOnly);

        in << _dataSelected.keys().count();

        for(QString& channel : _dataSelected.keys()) {

            QPair<QColor, QVector<QPointF> > channelData = _dataSelected.value(channel);

            in << channel;                    // имя канала
            in << channelData.first.name();   // цвет канала
            in << channelData.second.count(); // кол-во точек в канале

            for(int i = 0; i < channelData.second.count(); i++) {

                //QPointF pointToScene = gItem->mapToScene(_dataSelected.value(channel).at(i));
                in << channelData.second.at(i); // координаты точки в канале

            }


        }

        QMimeData* mimeData = new QMimeData;
        mimeData->setData("plot", data);

        QClipboard* clipboard = QApplication::clipboard();
        clipboard->setMimeData(mimeData);

    }
}

void PlotGraphicsView::paste()
{
    // забираем точки из буфера обмена

    QClipboard* clipboard = QApplication::clipboard();

    const QMimeData* mimeData = clipboard->mimeData();
    QByteArray data = mimeData->data("plot");

    QDataStream out(&data, QIODevice::ReadOnly);

    int size = 0;

    out >> size;

    QMap<QString, QPair<QColor, QVector<QPointF> > > pastePointsTemp;
    qreal leftBorder = -1;
    qreal rightBorder = -1;

    for(int i = 0; i < size; i++) {

        QString channel;
        QString color = "#FFFFFF";
        int cnt = 0;

        out >> channel;
        out >> color;
        out >> cnt;

        qDebug() << "key = " << channel << ", cnt = " << cnt;

        QVector<QPointF> pastedPoints;

        for(int j = 0; j < cnt; j++) {
            QPointF point;
            out >> point;


            // находим левую и правую границы

            if(leftBorder == -1) {
                leftBorder = point.x();
            }
            else if(point.x() < leftBorder) {
                    leftBorder = point.x();
            }

            if(rightBorder == -1) {
                rightBorder = point.x();
            }
            else if(point.x() > rightBorder) {
                    rightBorder = point.x();
            }

            pastedPoints.push_back(point);

        }

        pastePointsTemp.insert(channel, QPair<QColor, QVector<QPointF> > (QColor(color), pastedPoints));

    }

    pasteData(pastePointsTemp, leftBorder, rightBorder);
}

void PlotGraphicsView::pasteData(const QMap<QString, QPair<QColor, QVector<QPointF> > > &data, qreal leftBorder, qreal rightBorder)
{
    // снимаем выделение с точек

    onPasteCancel(); // очистка области вставки

    for(auto &item : _selectedItems) {
        ((PointItem*)item)->select(false);
    }
    _selectedItems.clear();

    _selectedPoint = nullptr;
    gItem->setSelectedPoint(_selectedPoint);

    // рисуем область вставки

    _pasteAreaItem->setRect(QRectF(QPointF(0, 0), QSizeF(gItem->boundingRect().width(),
                                                         gItem->boundingRect().height())));


    _pasteAreaItem->setPos(QPointF(0, 0));
    _pasteAreaItem->setZValue(10);

    _pasteAreaItem->show();


    qreal offset = 10; // отсутуп от границы области вставки

    // размер области вставки
    _pasteAreaItem->setRect(QRectF(QPointF(0, 0),
                                   QSizeF(offset * 2 + gItem->boundingRect().width() * (rightBorder - leftBorder) / 100.0,
                                   gItem->boundingRect().height())));

    // помещаем область вставки на середину видимой части графика
    QRectF viewRectScene = mapToScene(this->viewport()->rect()).boundingRect();  // видимая область сцены
    QRectF viewRectPlot = gItem->mapFromScene(viewRectScene).boundingRect();     // видимая область графика
    qreal xPos = (viewRectPlot.x() + viewRectPlot.width()*0.5) - _pasteAreaItem->boundingRect().width() * 0.5; // середина видимой области графика минус половина области вставки


    //qreal xPos = gItem->boundingRect().width() * leftBorder / 100 - 10;




    _pasteAreaItem->setPos(QPointF(xPos, 0));

    qreal realLeftBorder = gItem->boundingRect().width() * leftBorder / 100;
    qreal realRightBorder = gItem->boundingRect().width() * rightBorder / 100;

    QPointF leftBorderFromPlot = gItem->mapToScene(QPointF(realLeftBorder, 0));
    QPointF leftBorderFromScene = _pasteAreaItem->mapFromScene(leftBorderFromPlot); // левая граница  в системе координат области вставки

    // ----------------------

    // создаем копии точек и размещаем их на элементе вставки

    for(QString& key : data.keys()) {

        QVector<PointItem*> pastedPoints;
        QColor color = data.value(key).first;
        QVector<QPointF> points = data.value(key).second;

        if(points.count()) {

            // создаем первую точку
            const QPointF& point = points.at(0);

            qreal x = gItem->boundingRect().width() * point.x() / 100;
            qreal y = gItem->boundingRect().height() * point.y() / 100;

            QPointF pointFromPlot = gItem->mapToScene(QPointF(x, y));
            QPointF pointFromSceneFirst = _pasteAreaItem->mapFromScene(pointFromPlot); // крайняя точка в системе координат области вставки

            double diff = pointFromSceneFirst.x() - leftBorderFromScene.x(); // разница между положением точки и началом координат области втавки

            PointItem* newPointLeft = new PointItem(QPointF(offset + diff /*pointFromSceneFirst.x()*/, pointFromSceneFirst.y()), _pasteAreaItem);
            newPointLeft->setColor(color);

            // создаем остальные точки, следующие за первой
            for(int i = 0 ; i < points.count(); i++ ) {

                pastedPoints.push_back(newPointLeft);

                if(i + 1 < points.count()) {

                    const QPointF& point = points.at(i + 1);

                    qreal x = gItem->boundingRect().width() * point.x() / 100;
                    qreal y = gItem->boundingRect().height() * point.y() / 100;

                    QPointF pointFromPlot = gItem->mapToScene(QPointF(x, y));
                    QPointF pointFromScene = _pasteAreaItem->mapFromScene(pointFromPlot);

                    // перемещаем точку по x относительно начала координат, так чтобы она была на области вставки, а не за ее пределами
                    //pointFromScene = QPointF(pointFromScene.x() + diff, pointFromScene.y());
                    diff = pointFromScene.x() - leftBorderFromScene.x();

                    PointItem* newPointRight = new PointItem(QPointF(offset + diff, pointFromScene.y()), _pasteAreaItem);
                    newPointRight->setColor(color);

                    LineItem* line = new LineItem(newPointLeft, newPointRight, _pasteAreaItem);
                    line->setColor(color);

                    newPointLeft->setLineRight(line);
                    newPointRight->setLineLeft(line);

                    newPointLeft = newPointRight;

                }

            }

            _pasteChannelMap.insert(key, pastedPoints);
        }
    }



    emit pastePrepared(_pasteChannelMap.keys(), _channelMap.keys());
}

void PlotGraphicsView::setAlignment(AlignType type)
{
    if(type == AlignType::AlignHorizontal) {
        // выравнивание по горизонтали

        if(_selectedPoint != nullptr) {
            // если есть выделенная точка, то выравниваем по ней
            for(QGraphicsItem* item : _selectedItems) {
                item->setPos(QPointF(item->pos().x(), _selectedPoint->pos().y()));
            }
        }
        else {
            // если нет выделенной точки, то выравниваем по среднем значению Y всех точек

            double averageY = 0;

            for(QGraphicsItem* item : _selectedItems) {
                averageY += item->pos().y();
            }


            for(QGraphicsItem* item : _selectedItems) {
                item->setPos(QPointF(item->pos().x(), averageY / _selectedItems.count()));
            }
        }
    }
    else if (type == AlignType::AlignVertical) {
        // выравнивание по вертикали

        if(_selectedPoint != nullptr) {
            // если есть выделенная точка, то выравниваем по ней

            for(QGraphicsItem* item : _selectedItems) {
                item->setPos(QPointF(_selectedPoint->pos().x(), item->pos().y()));
            }
        }
        else {
            // если нет выделенной точки, то выравниваем по среднем значению X всех точек

            double averageX = 0;
            for(QGraphicsItem* item : _selectedItems) {
                averageX += item->pos().x();
            }


            for(QGraphicsItem* item : _selectedItems) {
                item->setPos(QPointF(averageX / _selectedItems.count(), item->pos().y()));
            }
        }
    }
}

/*
void PlotGraphicsView::getChannelValueForPos(const QPointF &pos)
{
    _channelMapForPos.clear();

    // проходим по всем присутствующим графикам
    for(QString& name : _channelMap.keys()) {

        Channel* channel = _channelMap.value(name);

        //QVector<QPointF> points = getData(name);

        // находим проекцию точки под курсором на график
        for(int i = 1; i < channel->_points.count(); i++) {
            PointItem* pointLeft = channel->_points.at(i - 1);
            PointItem* pointRight = channel->_points.at(i);

            if(pos.x() >= pointLeft->pos().x() && pos.x() <= pointRight->pos().x()) {
                qreal y = lightcoder::ScriptPanel::GetPointPojection(pos, pointLeft->pos(), pointRight->pos());

                qreal yPercent = 100 - (y * 100 / gItem->boundingRect().height()); // позиция точки в процентах

                _channelMapForPos.insert(name, scaleY->getScaleMaximum() * yPercent / 100);
            }
        }
    }

}
*/


void PlotGraphicsView::mouseDoubleClickEvent(QMouseEvent * event)
{

     QGraphicsView::mouseDoubleClickEvent(event);

     QPointF viewToScenePoint = mapToScene(event->pos());
     QPointF sceneToPlotPoint = gItem->mapFromScene(viewToScenePoint);

     QList<QGraphicsItem*> list = this->items(QRect(event->pos().x() - CURSOR_RECT * 0.5,
                                                    event->pos().y() - CURSOR_RECT * 0.5,
                                                    CURSOR_RECT, CURSOR_RECT));

     QList<QGraphicsItem*> lineList;

     for(auto &item : list) {

         if(item->type() == LineItem::Type) {
             // проверяем только элементы LineItem
             lineList.push_back(item);
         }
     }

     if(lineList.count()) {
         // если есть хоть одна линия

         for(auto& line : lineList) {
             // проходим по всем линиям, и ищем нужную

             LineItem* lineItem = (LineItem*)line;
             PointItem* p1 = lineItem->pointLeft();
             PointItem* p2 = lineItem->pointRight();

             for(Channel *channel : _channelMap.values()) {

                 if(channel != nullptr /*&& channel->_id == 0*/
                         && channel->_points.contains(p1) && channel->_points.contains(p2)) {
                     // если список точек для канала содержит в себе точки затронутой линии

                     qreal priority = lineItem->zValue(); // выставить такой же приоритет у новых линий и точки

                     this->scene()->removeItem(lineItem);

                     delete lineItem;

                     // Проверка, чтоб новая точка не заходила за пределы крайних точек
                     if(sceneToPlotPoint.x() < p1->x()) {
                         sceneToPlotPoint = QPointF(p1->x(), sceneToPlotPoint.y());
                     }

                     if(sceneToPlotPoint.x() > p2->x()) {
                         sceneToPlotPoint = QPointF(p2->x(), sceneToPlotPoint.y());
                     }


                     PointItem *pNew = new PointItem(sceneToPlotPoint, gItem);
                     pNew->setColor(channel->_color);
                     pNew->setZValue(priority + 1);

                     qDebug() << "mouseDoubleClickEvent" << sceneToPlotPoint;

                     LineItem *line1 = new LineItem(p1, pNew, gItem);
                     line1->setColor(channel->_color);
                     line1->setZValue(priority);
                     p1->setLineRight(line1);

                     LineItem *line2 = new LineItem(pNew, p2, gItem);
                     line2->setColor(channel->_color);
                     line2->setZValue(priority);
                     p2->setLineLeft(line2);

                     pNew->setLineLeft(line1);
                     pNew->setLineRight(line2);

                     //_pointVector.push_back(pNew);
                     // вставка новой точки между точками, кторые были концами линии
                     auto iterator = std::find_if(channel->_points.begin(), channel->_points.end(), [&](PointItem *p) {
                         return p->pos().x() == p2->x() && p->pos().y() == p2->y();
                     });

                     channel->_points.insert(iterator, pNew);

                     if (event->modifiers() & Qt::ControlModifier == false){
                         // убираем выделение с остальных и чистим список
                         for(auto &item : _selectedItems) {
                             ((PointItem*)item)->select(false);
                         }
                         _selectedItems.clear();
                     }

                     _selectedItems.insert(pNew);
                     pNew->select(true);

                     _selectedPoint = pNew;
                     gItem->setSelectedPoint(_selectedPoint);

                     // Позиция точки
                     qreal percentPlotX = pNew->pos().x() * 100.0 / gItem->boundingRect().width();
                     qreal xScale =  scaleX->getScaleMaximum() * percentPlotX / 100.0;

                     qreal percentPlotY = pNew->pos().y() * 100.0 / gItem->boundingRect().height();
                     qreal yScale =  scaleY->getScaleMaximum() * (100 - percentPlotY) / 100.0;


                     emit selectedPoint(xScale, yScale);

                     break;


                 }
             }


         }

     }


     gItem->update();

    _isPressed = true;


    emit store(); // for undo
}

void PlotGraphicsView::mousePressEvent(QMouseEvent *event)
{

    qDebug() << "mousePressEvent";

    if(event->button() == Qt::MouseButton::LeftButton) {

        _cursorPos = this->cursor().pos();


        QGraphicsView::mousePressEvent(event);

        _posStartX = event->x();
        _posStartY = event->y();

        QPointF pointScene = mapToScene(event->pos());

        QList<QGraphicsItem*> list = this->scene()->items(QRect(pointScene.x() - CURSOR_RECT * 0.5,
                                                       pointScene.y() - CURSOR_RECT * 0.5,
                                                       CURSOR_RECT, CURSOR_RECT));

        // проверяем, есть ли элемент среди выделенных


        _pointsUnderCursor.clear();

        for(auto &item : list) {

            //if(_cursorItem == nullptr || _cursorItem->isVisible() == false) {
                // если курсор видимый (из чего следует, что запущено проигрывание сцены),
                // то запрещаем перетаскивание точек

                if(item->type() == PointItem::Type) {
                    // проверяем только элементы PointItem

                    PointItem* selectedPointNew = (PointItem*)item;

                    for(QString key : _channelMap.keys()) {
                        Channel *channel = _channelMap.value(key);

                        if(channel->_points.contains(selectedPointNew)) {
                            // собираем точки под курсором
                            _pointsUnderCursor.insert(key, selectedPointNew);

                        }
                    }

                }

                if(item->type() == PasteAreaItem::Type) {
                    // если область вставки

                    for(auto &item : _selectedItems) {
                        ((PointItem*)item)->select(false);
                    }
                    _selectedItems.clear();

                    _selectedPoint = nullptr;
                    gItem->setSelectedPoint(_selectedPoint);

                    _isPasteAreaCatched = true;

                    QPointF viewToScenePoint = mapToScene(event->pos());
                    pasteAreaPos = _pasteAreaItem->mapFromScene(viewToScenePoint);

                    break;

                }
            //}

            if(item->type() == CursorItem::Type) {

                for(auto &item : _selectedItems) {
                    ((PointItem*)item)->select(false);
                }
                _selectedItems.clear();

                _selectedPoint = nullptr;
                gItem->setSelectedPoint(_selectedPoint);

                _isCursorCatched = true;

                //QPointF viewToScenePoint = mapToScene(event->pos());
                // cursorPos = _cursorItem->mapFromScene(viewToScenePoint);
                cursorPos = _cursorItem->mapFromScene(event->pos());

                //cursorPos = event->pos();

                //qDebug() << "cursor pos = " << cursorPos << viewToScenePoint;

                break;

            }
        }


        if(_pointsUnderCursor.keys().count() > 1) {
            // если точек больше чем одна, то пользователю предоставляется возможность выбора

            bool isExist = false;
            for(PointItem *point : _pointsUnderCursor.values()) {
                if(_selectedPoint == point) {
                    // если какая-то из точек под курсором уже выделена (selected), то точки можно перетаскивать
                    // в противном случае появится диалог выбора канала
                    isExist = true;
                    break;
                }

                if(_selectedItems.contains(point)) {
                    // если точки уже выделены (даже если они лежат друго под другом), то не нужно предлагать выбор (Николай так захотел)

                    isExist = true;

                    _selectedPoint = (PointItem*)point;
                    gItem->setSelectedPoint(point);

                    break;
                }
            }                        

            if(isExist == false) {

                // если среди выделенных нет таких точек

                QString selectedChannel;

                // меню выбора цвета (возвращает выбранный канал в selectedChannel)
                PointColorMenu pointColorMenu(selectedChannel);
                pointColorMenu.setColorList(_pointsUnderCursor);
                pointColorMenu.move(QPoint(QCursor::pos().x() + CURSOR_RECT, QCursor::pos().y()));
                pointColorMenu.setFocus();
                pointColorMenu.exec();

                PointItem* selectedItem = _pointsUnderCursor.value(selectedChannel);

                if(selectedItem != nullptr) {
                    mousePressed(event, selectedChannel, selectedItem);

                    qreal percentPlotXNew = selectedItem->pos().x() * 100.0 / gItem->boundingRect().width();
                    qreal xScaleNew =  scaleX->getScaleMaximum() * percentPlotXNew / 100.0;

                    qreal percentPlotYNew = 100 - selectedItem->pos().y() * 100.0 / gItem->boundingRect().height();
                    qreal yScaleNew =  scaleY->getScaleMaximum() * percentPlotYNew / 100.0;


                    emit selectedPoint(xScaleNew, yScaleNew);
                }

                return;
            }

        }
        else if(_pointsUnderCursor.keys().count() == 1){
            // если только одна точка
            QString selectedChannel = _pointsUnderCursor.keys().at(0);

            PointItem* selectedItem = _pointsUnderCursor.value(selectedChannel);

            mousePressed(event, selectedChannel, selectedItem);
        }
        else {

            if (event->modifiers() & Qt::ControlModifier) {
                // При нажатой правой клавише мыши + Ctrl

                _originPos = event->pos();
                _rubberBand->setGeometry(QRect(_originPos, QSize()));

                _rubberBand->show();
            }

        }


        if(_pointsUnderCursor.isEmpty() && _rubberBand->isVisible() == false/*_selectedPoint == nullptr*/) {
            // если нет в области курсора ни одной точки,
            // то убираем выделение с остальных и чистим список

            for(auto &item : _selectedItems) {

                ((PointItem*)item)->select(false);
            }
            _selectedItems.clear();

            _selectedPoint = nullptr;

            gItem->setSelectedPoint(_selectedPoint);

            _originPos = event->pos();

        }

        // возвращаем координаты выбранной точки
        if(_selectedItems.isEmpty() == false) {

            if(_selectedPoint != nullptr) {
                // Позиция точки
                qreal percentPlotX = _selectedPoint->pos().x() * 100.0 / gItem->boundingRect().width();
                qreal xScale =  scaleX->getScaleMaximum() * percentPlotX / 100.0;

                qreal percentPlotY = 100 - _selectedPoint->pos().y() * 100.0 / gItem->boundingRect().height();
                qreal yScale =  scaleY->getScaleMaximum() * percentPlotY / 100.0;

                emit selectedPoint(xScale, yScale);
            }
        }
        else {
            emit selectedPoint(-1, -1);
        }



        _isPressed = true;

        this->scene()->update();

        this->setCursor(Qt::ClosedHandCursor);
        qDebug() << "PlotGraphicsView::mousePressEvent setCursor";

        //this->releaseMouse();
    }
    else if(event->button() == Qt::MouseButton::RightButton) {

        if(_plotType == PlotType::PLOT_RGB) {

            QPointF viewToScenePoint = mapToScene(event->pos());
            _sceneToPlotPoint = gItem->mapFromScene(viewToScenePoint);

            channels_map.clear();

            // убираем выделение с остальных и чистим список
            for(auto &item : _selectedItems) {
                ((PointItem*)item)->select(false);
            }
            _selectedItems.clear();

            gItem->setSelectedPoint(nullptr);

            // проходим по всем присутствующим графикам
            for(QString& name : _channelMap.keys()) {

                Channel* channel = _channelMap.value(name);

                //QVector<QPointF> points = getData(name);

                // находим проекцию точки под курсором на график
                for(int i = 1; i < channel->_points.count(); i++) {
                    PointItem* pointLeft = channel->_points.at(i - 1);
                    PointItem* pointRight = channel->_points.at(i);

                    if(_sceneToPlotPoint.x() >= pointLeft->pos().x() && _sceneToPlotPoint.x() <= pointRight->pos().x()) {
                        qreal y = lightcoder::ScriptPanel::GetPointPojection(_sceneToPlotPoint, pointLeft->pos(), pointRight->pos());

                        qreal yPercent = 100 - (y * 100 / gItem->boundingRect().height()); // позиция точки в процентах

                        channels_map.insert(name, scaleY->getScaleMaximum() * yPercent / 100);

                        /*

                        // новая точка
                        QGraphicsLineItem* lineItem = pointLeft->lineRight();

                        this->scene()->removeItem(lineItem);

                        delete lineItem;


                        PointItem *pNew = new PointItem(QPointF(_sceneToPlotPoint.x(), y), gItem);
                        pNew->setColor(channel->_color);

                        LineItem *line1 = new LineItem(pointLeft, pNew, gItem);
                        line1->setColor(channel->_color);
                        line1->setZValue(channel->_id);
                        pointLeft->setLineRight(line1);

                        LineItem *line2 = new LineItem(pNew, pointRight, gItem);
                        line2->setColor(channel->_color);
                        line2->setZValue(channel->_id);
                        pointRight->setLineLeft(line2);

                        pNew->setLineLeft(line1);
                        pNew->setLineRight(line2);

                        //_pointVector.push_back(pNew);
                        // втсавка новой точки между точками, кторые были концами линии
                        auto iterator = std::find_if(channel->_points.begin(), channel->_points.end(), [&](PointItem *p) {
                            return p->pos().x() == pointRight->x() && p->pos().y() == pointRight->y();
                        });

                        channel->_points.insert(iterator, pNew);

                        _selectedItems.insert(pNew);
                        pNew->select(true);

                        _selectedPoint = pNew;
                        gItem->setSelectedPoint(_selectedPoint);
                        */

                        break;

                    }
                }
            }

            color_picker_->SetValues(&channels_map);
            color_picker_->show();
            color_picker_->move(QPoint(QCursor::pos().x() + CURSOR_RECT, QCursor::pos().y()));

            //_pointColorMenu->show();
            //_pointColorMenu->move(QPoint(QCursor::pos().x() + CURSOR_RECT, QCursor::pos().y()));
            //_pointColorMenu->setFocus();

        }

    }

}

void PlotGraphicsView::mouseMoveEvent(QMouseEvent *event)
{

    if(_isPressed) {

        if ( _rubberBand->isVisible() && event->modifiers() & Qt::ControlModifier) {
            // область выделения
            _rubberBand->setGeometry(QRect(_originPos, event->pos()).normalized());

            //_rubberBand->setGeometry(QRect(QPoint(_originPos.x(), 40), QPoint(event->pos().x(), this->rect().height() - 40)));

            return;
        }
        else {
            _rubberBand->hide();
        }


        if(_selectedItems.isEmpty() == false) {


            if(_selectedPoint != nullptr) {

                QPointF viewToScenePoint = mapToScene(event->pos());
                QPointF newPoint = gItem->mapFromScene(viewToScenePoint);
                _diffPoint = newPoint - _selectedPoint->pos(); // Разница между старой позицией и новой позицией точки, которая находится под курсором

                // Проверка, согласны ли остальные точки переместиться на указанной расстояние от своего нынешнего положения
                for(auto &item : _selectedItems) {

                    PointItem *point = (PointItem*)item;
                    QPointF posNew = point->pos() + _diffPoint; // новая позиция точки

                    checkPointForBorders(posNew, point);  // проверка выхода за границы
                    checkPointForNeighbor(posNew, point); // проверка выхода за пределы соседних точек

                    _diffPoint = posNew - point->pos(); // расстояние, на которе можно переместить точки, с учетом возможностей всех точек на перемещение

                }

                if(event->modifiers() & Qt::AltModifier) {
                    // если зажат Alt, то запрещаем перетаскивание по Y
                    _diffPoint = QPointF(_diffPoint.x(), 0);

                    // запрещаем курсору двигаться по вертикали
                    QCursor cursor = this->cursor();
                    _cursorPos = QPoint(cursor.pos().x(), _cursorPos.y());
                    cursor.setPos(_cursorPos);
                    this->setCursor(cursor);


                }
                else if(event->modifiers() & Qt::ShiftModifier) {
                    // если зажат Shift, то запрщаем перетасивание по X
                    _diffPoint = QPointF(0, _diffPoint.y());

                    // запрещаем курсору двигаться по горизонтали
                    QCursor cursor = this->cursor();
                    _cursorPos = QPoint(_cursorPos.x(), cursor.pos().y());
                    cursor.setPos(_cursorPos);
                    this->setCursor(cursor);
                }
                else {
                    // если не зажат Alt или Shift, то просто запоминаем позицию курсора
                    // (на случай, если в момент перетаскивания пользователь зажмет Alt или Shift)

                    QCursor cursor = this->cursor();
                    _cursorPos = QPoint(cursor.pos().x(), cursor.pos().y());
                }

                for(auto &point : _selectedItems) {
                    // перемещаем точки
                    point->setPos(point->pos() + _diffPoint);
                }

                if(_diffPoint.x() != 0 || _diffPoint.y() != 0) {
                    _isMoved = true;
                }
            }


        }
        else if(_isPasteAreaCatched) {
            // если пользователь тащит область выделения

            QPointF viewToScenePoint = mapToScene(event->pos());
            QPointF newPoint = gItem->mapFromScene(viewToScenePoint);

            if(_pasteAreaItem != nullptr) {

                QPointF newPos = QPointF(newPoint.x() - pasteAreaPos.x(), /*newPoint.y() - pasteAreaPos.y()*/ _pasteAreaItem->pos().y());

                _pasteAreaItem->setPos(newPos);

                // прилипание к границе (+-5px)

                for(const QString& key: _pasteChannelMap.keys()) {
                    // проходим по каналам области вставки

                    QVector<PointItem*> points = _pasteChannelMap.value(key);

                    for(PointItem *pastePoint : points) {
                        QPointF pointToPlot = gItem->mapFromItem(_pasteAreaItem, pastePoint->pos());


                        if(pointToPlot.x() >= gItem->boundingRect().width() - 5 && pointToPlot.x() <= gItem->boundingRect().width() + 5) {
                            // если одна из точек приблизилась к границе графика, то эта точка будет прилипать к правой границе (смещая всю область вставки)

                            double delta = gItem->boundingRect().width() - pointToPlot.x();
                            QPointF newPos = QPointF(_pasteAreaItem->pos().x() + delta, /*newPoint.y() - pasteAreaPos.y()*/ _pasteAreaItem->pos().y());
                            _pasteAreaItem->setPos(newPos);

                        }

                        if(pointToPlot.x() >= -5 && pointToPlot.x() <= 5) {
                            // если одна из точек приблизилась к границе графика, то эта точка будет прилипать к левой границе (смещая всю область вставки)

                            double delta = -pointToPlot.x();
                            QPointF newPos = QPointF(_pasteAreaItem->pos().x() + delta, /*newPoint.y() - pasteAreaPos.y()*/ _pasteAreaItem->pos().y());
                            _pasteAreaItem->setPos(newPos);

                        }
                    }
                }
            }
        }
        else if(_isCursorCatched) {
            // если пользователь тащит курсор

            //QPointF viewToScenePoint = mapToScene(event->pos());

            //qDebug() << "_isCursorCatched = " << event->pos() << viewToScenePoint;

            //QPointF newPoint = gItem->mapFromScene(viewToScenePoint);
            //QPointF diffPoint = viewToScenePoint - _cursorItem->pos(); // Разница между старой позицией и новой элемента, который находится под курсором


            if(_cursorItem != nullptr) {

                QPointF newPos = QPointF(event->pos().x() - cursorPos.x(), _cursorItem->pos().y());

                if(newPos.x() > scaleY->pos().x() + scaleY->rect().width()
                        && newPos.x() < scaleVertical->pos().x() + scaleVertical->rect().x()) {
                    // проверяем, чтобы курсор не заходил за шкалу (слева и справа)

                    _cursorItem->setPos(newPos);

                    //getCursorPosition();
                    emit CursorMoved();
                }


            }

        }
        else {
            // если не выбрана ни одна точка, то перетаскиваем сцену, прокручивая скроллы
            horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (event->x() - _posStartX));
            verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - _posStartY));
            _posStartX = event->x();
            _posStartY = event->y();

        }
    }
    else {


        /*
        if(_pasteAreaItem->isVisible()) {

            QRectF pasteAreaRect = _pasteAreaItem->mapToScene(_pasteAreaItem->rect()).boundingRect();

            setCursorForRect(QPointF(event->pos()), pasteAreaRect);
        }*/
        //else {
            this->setCursor(Qt::OpenHandCursor);
        //}

    }

    this->scene()->update();

    if(_selectedItems.isEmpty() == false) {

        if(_selectedPoint != nullptr) {
            // Позиция точки
            qreal percentPlotX = _selectedPoint->pos().x() * 100.0 / gItem->boundingRect().width();
            qreal xScale =  scaleX->getScaleMaximum() * percentPlotX / 100.0;

            qreal percentPlotY = 100 - _selectedPoint->pos().y() * 100.0 / gItem->boundingRect().height();
            qreal yScale =  scaleY->getScaleMaximum() * percentPlotY / 100.0;

            emit selectedPoint(xScale, yScale);
        }        
    }
    else {
        emit selectedPoint(-1, -1);


    }

    QGraphicsView::mouseMoveEvent(event);

    this->scene()->update();

    if(_plotType == PlotType::PLOT_RGB)
        onPointChanged();

}

void PlotGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent(event);

    if (_rubberBand->isVisible() ) {

        _rubberBand->hide();

        if(event->pos() != _originPos && event->modifiers() & Qt::ControlModifier) {

            QPolygonF polygonScene = this->mapToScene(_rubberBand->geometry());

            QList<QGraphicsItem*> list = this->scene()->items(polygonScene);

            /*
            // законмментированно, поскольку не нужно чистить список при вылелении области
            // чистим все выделенные
            for(auto &item : _selectedItems) {

                ((PointItem*)item)->select(false);
            }
            _selectedItems.clear();
            */

            if(event->modifiers() & Qt::AltModifier) {
                // если зажат Alt, то удаляем из выделенных

                for(auto &item : list) {

                    if(item->type() == PointItem::Type) {
                        // проверяем только элементы PointItem

                        if(_selectedItems.contains(item)) {
                            // если среди выделенных есть такой элемент, то удаляем его

                            ((PointItem*)item)->select(false);
                            _selectedItems.remove(item);

                            if(gItem->selectedPoint() == item) {
                                //  если точка выделены на графике
                                gItem->setSelectedPoint(nullptr);
                            }
                        }

                    }
                }



            }
            else {
                // запоминаем все точки в выделенном поле
                for(auto &item : list) {

                    if(item->type() == PointItem::Type) {
                        // проверяем только элементы PointItem

                        ((PointItem*)item)->select(true);
                        _selectedItems.insert(item);

//                        _selectedPoint = (PointItem*)item;
//                        gItem->setSelectedPoint(_selectedPoint);

                    }
                }
            }
        }
    }

    if(_isMoved) {
        // если перемещали точку, то отсылаем сигнал для undo-стека

        _isMoved = false;

        emit store();
    }

    _isPressed = false;
    _isCursorCatched = false;
    _isPasteAreaCatched = false;

    this->setCursor(Qt::OpenHandCursor);

    qDebug() << "PlotGraphicsView::mouseReleaseEvent setCursor";

    this->update();
}

void PlotGraphicsView::keyPressEvent(QKeyEvent *event)
{

    if(event->key() == Qt::Key::Key_Delete) {

        for(auto &item: _selectedItems) {

            PointItem* point = (PointItem*)item;

            for(Channel *channel :_channelMap.values()) {

                if(channel == nullptr || channel->_points.contains(point) == false) {
                    continue;
                }

                point->select(false);

                PointItem* pointLeft = nullptr;
                PointItem* pointRight = nullptr;

                LineItem* lineLeft = (LineItem*)point->lineLeft();

                if(lineLeft != nullptr) {
                    pointLeft = lineLeft->pointLeft();


                    if(pointLeft == nullptr) {
                        continue;
                    }

                }
                else {
                    continue;
                }

                LineItem* lineRight = (LineItem*)point->lineRight();

                if(lineRight != nullptr) {
                    pointRight = lineRight->pointRight();


                    if(pointRight == nullptr) {
                        continue;
                    }
                }
                else {
                    continue;
                }

                this->scene()->removeItem(point);
                this->scene()->removeItem(lineLeft);
                this->scene()->removeItem(lineRight);

                delete point;
                delete lineLeft;
                delete lineRight;

                LineItem* lineNew = new LineItem(pointLeft, pointRight, gItem);
                lineNew->setColor(channel->_color);
                lineNew->setZValue(channel->_id);

                if(pointLeft != nullptr) {
                    pointLeft->setLineRight(lineNew);
                }

                if(pointRight != nullptr) {
                    pointRight->setLineLeft(lineNew);
                }

                //_pointVector.removeOne(point);
                channel->_points.removeOne(point);



                break;

            }

        }

        if(_selectedItems.isEmpty() == false) {
            emit store(); // for undo-stack
        }


        _selectedItems.clear();

        _selectedPoint = nullptr;
        gItem->setSelectedPoint(_selectedPoint);

        this->scene()->update();
    }
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key::Key_C) {
        // копирование (CTRL + C)

        copy();

        //qDebug() << "CTRL + C";

    }
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key::Key_V) {
        // вставка (CTRL + V)

        paste();

        //qDebug() << "CTRL  V";

    }


}

void PlotGraphicsView::keyReleaseEvent(QKeyEvent *event)
{

}

void PlotGraphicsView::mousePressed(QMouseEvent* event, const QString& name, PointItem* point)
{

    // зададим Z-значение для точек и линий графика, так, чтобы выделенный график находился поверх остальных
    int mainPriority = _channelMap.keys().count() - 1;    // приоритет для выбранного графика
    int priority = mainPriority - 1;                      // приоритет остальных графиков

    for(const QString key : _channelMap.keys()) {

        Channel *channel = _channelMap.value(key);

        if(channel != nullptr) {

            if(key == name) {
                // ставим высший приоритет для точек и линий выбранного графика

                for(PointItem* point : channel->_points) {
                    point->setZValue(mainPriority);
                    QGraphicsLineItem* line = point->lineRight();
                    if(line != nullptr) {
                        line->setZValue(mainPriority - 1);
                    }
                }
            }
            else {
                priority--;

                for(PointItem* point : channel->_points) {
                    point->setZValue(priority);
                    QGraphicsLineItem* line = point->lineRight();
                    if(line != nullptr) {
                        line->setZValue(priority - 1);
                    }
                }
            }
        }

    }
    this->scene()->update();


    if(event->modifiers() & Qt::ControlModifier) {
        // если зажат Ctrl

        if(_selectedItems.contains(point)) {
            // если есть такой элемент в списке выделенных,
            // то удаляем его из списка выделенных

            point->select(false);

            _selectedItems.remove(point);

            _selectedPoint = nullptr;
             gItem->setSelectedPoint(_selectedPoint);

             //return;

        }
        else {
            // если нет такого элемента в списке выделенных,
            // то добавляем его

            _selectedItems.insert(point);

            point->select(true);

            _selectedPoint = point;
            gItem->setSelectedPoint(_selectedPoint);

        }

    } else if(event->modifiers() & Qt::ShiftModifier) {
        // если зажата клавиша Shift

        if(_selectedPoint != nullptr) {
            // если есть ранее выбранная точка, то начинаем от нее шагать в стороону новой точки
            // и выделять все точки по пути

            if(point->pos().x() >= _selectedPoint->pos().x()) {
                // шагаем вправо
                // до тех пор, пока не встретим выделенную точку

                while(_selectedPoint != point) {
                    LineItem *lineRight = (LineItem*)_selectedPoint->lineRight();

                    if(lineRight != nullptr) {
                        PointItem *pointRight = lineRight->pointRight();

                        if(pointRight != nullptr) {
                            pointRight->select(true);

                            _selectedItems.insert(pointRight);

                            _selectedPoint = pointRight;
                        }
                        else {
                            // если дальше некуда продвигаться - заканчиваем
                            break;
                        }
                    }
                    else {
                        // если дальше некуда продвигаться - заканчиваем
                        break;
                    }
                }


            }
            else {
                // шагаем влево
                // до тех пор, пока не встретим выделенную точку

                while(_selectedPoint != point) {
                    LineItem *lineLeft = (LineItem*)_selectedPoint->lineLeft();

                    if(lineLeft != nullptr) {
                        PointItem *pointLeft = lineLeft->pointLeft();

                        if(pointLeft != nullptr) {
                            pointLeft->select(true);

                            _selectedItems.insert(pointLeft);

                            _selectedPoint = pointLeft;
                        }
                        else {
                            // если дальше некуда продвигаться - заканчиваем
                            break;
                        }
                    }
                    else {
                        // если дальше некуда продвигаться - заканчиваем
                        break;
                    }
                }
            }

            gItem->setSelectedPoint(_selectedPoint); // задаем новую выделенную точку для подсветки координат

        }
        else {
            // если еще нет выделенной точки, то выделяем

            _selectedItems.insert(point);

            point->select(true);

            _selectedPoint = point;
            gItem->setSelectedPoint(_selectedPoint);
        }

    }
    else {
        // в любом другом случае, когда не зажат Ctrl или Shift

        if(_selectedItems.contains(point)) {
            // если есть такой элемент в списке выделенных

            qDebug() << "contains";

            // если Ctrl не зажат, то выбираем эту точку как активную
            _selectedPoint = point;
            gItem->setSelectedPoint(_selectedPoint);

        }
        else {
            // если нет среди выделенных
            // то убираем выделение с остальных и чистим список

            qDebug() << "no contains";

            for(auto &item : _selectedItems) {
                ((PointItem*)item)->select(false);
            }
            _selectedItems.clear();

            _selectedItems.insert(point);

            _selectedPoint = point;
            _selectedPoint->select(true);
            gItem->setSelectedPoint(_selectedPoint);

        }
    }
}
